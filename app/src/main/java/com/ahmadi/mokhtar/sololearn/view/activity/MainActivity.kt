package com.ahmadi.mokhtar.sololearn.view.activity

import android.os.Build
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.ahmadi.mokhtar.sololearn.App
import com.ahmadi.mokhtar.sololearn.R
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.view.adapter.NewsAdapter
import com.ahmadi.mokhtar.sololearn.view.adapter.NewsSavedAdapter
import com.ahmadi.mokhtar.sololearn.view.base.BaseActivity
import com.ahmadi.mokhtar.sololearn.view.fragment.NewsDetailFragment
import com.ahmadi.mokhtar.sololearn.view.fragment.NewsFragment

class MainActivity : BaseActivity() , NewsAdapter.Callback ,NewsSavedAdapter.Callback{
    override fun initBeforeView() {
        with(application as App){
            di.inject(this@MainActivity)
        }
    }

    override fun getContentViewId(): Int = R.layout.activity_main;

    override fun initViews() {
        supportFragmentManager.
                beginTransaction().
                add(R.id.root_layout,NewsFragment.newInstance(this,this),"news").
                commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onItemClick(item: Results, imageView: ImageView, textView: TextView) {
           supportFragmentManager.
            beginTransaction().
            add(R.id.root_layout, NewsDetailFragment.newInstance(item),"news").
            addSharedElement(imageView,imageView.transitionName).
            addSharedElement(textView,textView.transitionName).
            addToBackStack(javaClass.simpleName).
            commit()
    }


}


