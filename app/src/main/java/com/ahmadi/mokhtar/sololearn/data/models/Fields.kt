package com.ahmadi.mokhtar.sololearn.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fields (
    val headline:String?,
    val starRating:String?,
    val shortUrl:String? ,
    val thumbnail:String?
):Parcelable