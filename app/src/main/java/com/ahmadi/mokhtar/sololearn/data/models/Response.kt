package com.ahmadi.mokhtar.sololearn.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Response (
    val status: String,
    val userTier: String,
    val total:Int,
    val startIndex:Int,
    val pageSize:Int,
    val currentPage:Int,
    val pages:Int? = 0,
    val orderBy:String,
    val results: List<Results>
): Parcelable