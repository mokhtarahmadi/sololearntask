package com.ahmadi.mokhtar.sololearn.data.database

import com.ahmadi.mokhtar.sololearn.data.models.Results
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface NewsRepo {

    fun isSaveResult(id:String, isSave:Boolean):Completable

    fun allResults(limit: Int, offset: Int): Observable<List<Results>>

    fun periodResults(): Single<List<Results>>;

    fun allResultSaved(limit: Int, offset: Int, isSaved:Boolean): Single<List<Results>>
}