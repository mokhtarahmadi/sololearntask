package com.ahmadi.mokhtar.sololearn.di.component

import androidx.annotation.Keep
import com.ahmadi.mokhtar.sololearn.di.module.ApiModule
import com.ahmadi.mokhtar.sololearn.di.module.AppModule
import com.ahmadi.mokhtar.sololearn.di.module.WorkerModule
import com.ahmadi.mokhtar.sololearn.view.activity.MainActivity
import com.ahmadi.mokhtar.sololearn.viewModel.NewsDetailFragmentViewModel
import com.ahmadi.mokhtar.sololearn.viewModel.NewsFragmentViewModel
import com.ahmadi.mokhtar.sololearn.worker.NewsWorkerFactory
import dagger.Component
import javax.inject.Singleton

@Keep
@Singleton
@Component(modules = [AppModule::class , ApiModule::class, WorkerModule::class])
interface DIComponent {
    interface Injectable{
        fun inject(diComponent: DIComponent)
    }

    fun inject(mainActivity: MainActivity)
    fun inject(newsFragmentViewModel: NewsFragmentViewModel)
    fun inject(newsDetailFragmentViewModel: NewsDetailFragmentViewModel)
    fun inject(): NewsWorkerFactory
}