package com.ahmadi.mokhtar.sololearn.data.network

import com.ahmadi.mokhtar.sololearn.data.models.Guardian
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("search")
    fun getPeriodSearchData(@Query("api-key") apiKey: String, @Query("show-fields") showFields:String,
                        @Query("from-date") fromDate:String, @Query("to-date") toDate:String,
                        @Query("show-tags") showTags:String, @Query("order-by") orderBy:String): Single<Guardian>

    @GET("search")
    fun getSearchData(@Query("api-key") apiKey: String, @Query("show-tags") showTags:String,
                      @Query("show-fields") showFields:String,@Query("order-by") orderBy:String,
                      @Query("page") page:Int): Observable<Guardian>

}