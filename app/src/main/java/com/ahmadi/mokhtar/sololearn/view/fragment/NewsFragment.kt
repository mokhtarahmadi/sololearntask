package com.ahmadi.mokhtar.sololearn.view.fragment

import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ahmadi.mokhtar.sololearn.App
import com.ahmadi.mokhtar.sololearn.R
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.utils.Constants
import com.ahmadi.mokhtar.sololearn.utils.InfiniteScrollListener
import com.ahmadi.mokhtar.sololearn.view.adapter.NewsAdapter
import com.ahmadi.mokhtar.sololearn.view.adapter.NewsSavedAdapter
import com.ahmadi.mokhtar.sololearn.view.base.BaseFragment
import com.ahmadi.mokhtar.sololearn.viewModel.NewsFragmentViewModel
import com.ahmadi.mokhtar.sololearn.viewModel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.fragment_news_detail.*
import org.w3c.dom.Text

class NewsFragment:BaseFragment() {

    val IMAGE_URI = "https://i.scdn.co/image/8d5eabf813797aa39f6e8186f702a1998d12fe40"
    private lateinit var viewModel: NewsFragmentViewModel
    private var newsSaevdAdapter: NewsSavedAdapter? = NewsSavedAdapter(ArrayList<Results>(),callbackNewsSaved!!)
    private var newsAdapter: NewsAdapter? = NewsAdapter(ArrayList<Results>(),callbackNews!!)
    private var currentPageNews = 0
    private var currentPageNewsSaved = 0
    private var txtSaved: TextView? = null


    companion object {
        var callbackNews: NewsAdapter.Callback? = null
        var callbackNewsSaved: NewsSavedAdapter.Callback? = null
        fun newInstance(callbackNews: NewsAdapter.Callback , callbackNewsSaved: NewsSavedAdapter.Callback): NewsFragment {
            this.callbackNews = callbackNews
            this.callbackNewsSaved = callbackNewsSaved
            return NewsFragment()
        }
    }

    override fun initBeforeView() {
        with(context!!.applicationContext as App){
            viewModel = androidx.lifecycle.ViewModelProviders.of(this@NewsFragment , ViewModelFactory(this)).get(
                NewsFragmentViewModel::class.java)

        }

    }

    override fun getContentViewId(): Int = R.layout.fragment_news

    override fun initViews(rootView: View) {
        initNewsRecyclerView()
        txtSaved = rootView.findViewById(R.id.txtSave) as TextView
        progressBar.visibility = View.VISIBLE
        initNewsSavedRecyclerView()
        viewModelObserver()
        LoadNewsSaved()
        loadNews()
        viewModel.loadDataFromWorker()
    }

    fun initNewsRecyclerView(){

        newsRecyclerView!!.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        newsRecyclerView.apply {
            setHasFixedSize(true)
            addOnScrollListener(
                InfiniteScrollListener({ loadNews() },
                newsRecyclerView!!.layoutManager as LinearLayoutManager
            )
            )
        }

    }


    fun initNewsSavedRecyclerView(){
        saveRecyclerView!!.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)

        saveRecyclerView.apply {
            setHasFixedSize(true)
            addOnScrollListener(
                InfiniteScrollListener({ LoadNewsSaved() },
                    saveRecyclerView!!.layoutManager as LinearLayoutManager
                )
            )
        }
    }
    private fun loadNews() {
        viewModel.loadNews(Constants.LIMIT,currentPageNews * Constants.OFFSET)
        currentPageNews++;
    }

    private fun LoadNewsSaved(){
        viewModel.getNewsSaved(Constants.LIMIT,currentPageNewsSaved * Constants.OFFSET)
    }

    fun viewModelObserver(){

        viewModel.newsResult().observe(this, Observer<List<Results>> {
            if (it != null) {
                val position = newsAdapter?.itemCount
                newsAdapter?.addNews(it)
                newsRecyclerView.adapter = newsAdapter
                if (position != null) {
                    newsRecyclerView.scrollToPosition(position - Constants.LIST_SCROLLING)
                }
            }
        })

        viewModel.newsLoader().observe(this, Observer<Boolean> {
            if (it == false) progressBar.visibility = View.GONE
        })

        viewModel.newsError().observe(this, Observer<String> {
            if (it != null) {
                Toast.makeText(activity,"error",Toast.LENGTH_LONG)
            }
        })


        viewModel.newsResultSaved().observe(this, Observer<List<Results>> {
            if (it != null && it.size>0) {
                txtSaved?.visibility = View.VISIBLE
                saveRecyclerView.visibility = View.VISIBLE
                val position = newsSaevdAdapter?.itemCount
                newsSaevdAdapter?.addNews(it)
                saveRecyclerView.adapter = newsSaevdAdapter
                if (position != null) {
                    saveRecyclerView.scrollToPosition(position - Constants.LIST_SCROLLING)
                }
            }

        })



        viewModel.newsSavedError().observe(this, Observer<String> {
            if (it != null) {
                Toast.makeText(activity,"error",Toast.LENGTH_LONG)
            }
        })


    }



    override fun onDestroy() {
        viewModel.disposeElements()
        super.onDestroy()
    }

}