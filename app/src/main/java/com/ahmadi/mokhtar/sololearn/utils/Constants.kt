package com.ahmadi.mokhtar.sololearn.utils

class Constants {
    companion object {

        const val BASE_URL =  "https://content.guardianapis.com/"

        const val ENG_LANG = "en"
        const val FA_LANG = "fa"
        const val AM_LANG = "hy"
        const val OFFSET = 10
        const val LIMIT = 10
        const val START_ZERO_VALUE = "0"
        const val DATABASE_NAME = "news_database.db"
        const val LIST_SCROLLING = 10
        const val PREF_NAME = "mokhtar_pref"
        const val EMPTY_EMAIL_ERROR = 1001
        const val INVALID_EMAIL_ERROR = 1002
        const val EMPTY_PASSWORD_ERROR = 1003
        const val LOGIN_FAILURE = 1004
        const val NULL_INDEX = -1L
        @JvmField val VERBOSE_NOTIFICATION_CHANNEL_NAME: CharSequence =
            "Verbose WorkManager Notifications"
        const val VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION =
            "Shows notifications whenever work starts"
        const val CHANNEL_ID = "VERBOSE_NOTIFICATION"
        const val NOTIFICATION_ID = 1

    }

    enum class LoggedInMode constructor(val type : Int){
        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_SERVER(1)
    }
}