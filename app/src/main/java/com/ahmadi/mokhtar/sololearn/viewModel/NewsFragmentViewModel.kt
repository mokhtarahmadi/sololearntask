package com.ahmadi.mokhtar.sololearn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.ahmadi.mokhtar.sololearn.data.database.NewsRepo
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.di.component.DIComponent
import com.ahmadi.mokhtar.sololearn.utils.iomain
import com.ahmadi.mokhtar.sololearn.worker.NewsWorker
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NewsFragmentViewModel : ViewModel(),DIComponent.Injectable {

    @Inject
    lateinit var newsRepo: NewsRepo
    private lateinit var disposableObserver: DisposableObserver<List<Results>>
    protected val disposables: CompositeDisposable = CompositeDisposable()
    var newsResult: MutableLiveData<List<Results>> = MutableLiveData()
    var newsError: MutableLiveData<String> = MutableLiveData()
    var newsLoader: MutableLiveData<Boolean> = MutableLiveData()
    var newsResultSaved: MutableLiveData<List<Results>> = MutableLiveData()
    var newsSavedError: MutableLiveData<String> = MutableLiveData()

    override fun inject(diComponent: DIComponent) {
        diComponent.inject(this)
    }



    fun newsResult(): LiveData<List<Results>> {
        return newsResult
    }

    fun newsError(): LiveData<String> {
        return newsError
    }

    fun newsLoader(): LiveData<Boolean> {
        return newsLoader
    }


    fun newsResultSaved(): LiveData<List<Results>> {
        return newsResultSaved
    }

    fun newsSavedError(): LiveData<String> {
        return newsSavedError
    }



    fun loadNews(limit: Int, offset: Int) {
        disposableObserver = object : DisposableObserver<List<Results>>() {
            override fun onComplete() {
            }

            override fun onNext(t: List<Results>) {
                newsResult.postValue(t)
                newsLoader.postValue(false)
            }

            override fun onError(e: Throwable) {
                newsError.postValue(e.message)
                newsLoader.postValue(false)
            }

        }


        newsRepo.allResults(limit, offset).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(disposableObserver)

    }

    fun getNewsSaved(limit: Int, offset: Int){

        disposables.add(newsRepo.allResultSaved(limit,offset,true).iomain()
            .subscribe({
                newsResultSaved.postValue(it)
            },{
                newsSavedError.postValue(it.message)
            }))
    }




        fun disposeElements(){
            if (!disposableObserver.isDisposed) disposableObserver.dispose()
            if (!disposables.isDisposed) disposables.dispose()
        }


    fun loadDataFromWorker(){
        WorkManager.getInstance().enqueueUniquePeriodicWork(
            "NewsWorker",
            ExistingPeriodicWorkPolicy.REPLACE,
            PeriodicWorkRequestBuilder<NewsWorker>(15, TimeUnit.MINUTES).build())

    }
}


