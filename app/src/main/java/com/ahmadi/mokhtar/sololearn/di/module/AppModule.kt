package com.ahmadi.mokhtar.sololearn.di.module

import android.content.Context
import androidx.room.Room
import com.ahmadi.mokhtar.sololearn.App
import com.ahmadi.mokhtar.sololearn.data.database.AppDatabase
import com.ahmadi.mokhtar.sololearn.data.database.NewsDao
import com.ahmadi.mokhtar.sololearn.data.database.NewsRepo
import com.ahmadi.mokhtar.sololearn.data.database.NewsRepository
import com.ahmadi.mokhtar.sololearn.data.network.Api
import com.ahmadi.mokhtar.sololearn.utils.Constants
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    fun providesApp():App = app

    @Provides
    @Singleton
    internal fun providesContext(): Context = app.applicationContext

    @Singleton
    @Provides
    internal fun providesAppDatabase(context: Context) : AppDatabase =
        Room.databaseBuilder(context , AppDatabase::class.java , Constants.DATABASE_NAME).build()

    @Provides
    @Singleton
    internal fun providesNewsDaoHelper(appDatabase: AppDatabase): NewsDao = appDatabase.getNewsDao()

    @Provides
    @Singleton
    internal fun providesNewsRepoHelper(appDatabase: AppDatabase, api: Api, context: Context): NewsRepo = NewsRepository(appDatabase.getNewsDao(),api,context)

    @Provides
    internal fun providesCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}