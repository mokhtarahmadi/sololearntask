package com.ahmadi.mokhtar.sololearn.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "results")
data class Results(
    @PrimaryKey
    val id:String,
    val type:String?,
    val sectionId:String?,
    val sectionName:String?,
    val webPublicationDate:String?,
    val webTitle:String?,
    val webUrl:String?,
    val apiUrl:String?,
    val fields:Fields? =null,
    val tags:List<Tag>? = null,
    val isHosted:String?,
    val pillarId:String?,
    val pillarName:String?,
    var isSave:Boolean = false
):Parcelable