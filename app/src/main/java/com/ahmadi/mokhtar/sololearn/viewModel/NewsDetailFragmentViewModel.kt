package com.ahmadi.mokhtar.sololearn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ahmadi.mokhtar.sololearn.data.database.NewsRepo
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.di.component.DIComponent
import com.ahmadi.mokhtar.sololearn.utils.iomain
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class NewsDetailFragmentViewModel : ViewModel(),DIComponent.Injectable {


    @Inject
    lateinit var newsRepo: NewsRepo
    protected val disposables: CompositeDisposable = CompositeDisposable()
    var isSaveError: MutableLiveData<String> = MutableLiveData()
    var isSaveResult: MutableLiveData<Boolean> = MutableLiveData()

    override fun inject(diComponent: DIComponent) {
        diComponent.inject(this)
    }

    fun isSaveError(): LiveData<String> {
        return isSaveError
    }

    fun isSaveResult(): LiveData<Boolean> {
        return isSaveResult
    }


    fun isSaveNews(id:String, isSave:Boolean){

        disposables.add(newsRepo.isSaveResult(id, isSave).iomain().subscribe(
            {
                isSaveResult.postValue(true)
            },
            {
                isSaveError.postValue(it.message)
            }
        ))

    }

    fun disposeElements(){
        if (!disposables.isDisposed) disposables.dispose()
    }


}