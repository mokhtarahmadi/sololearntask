package com.ahmadi.mokhtar.sololearn.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ahmadi.mokhtar.sololearn.data.models.Results
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertResults(results: Results)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllResults(results: List<Results>)

    @Query("SELECT * FROM results WHERE id = :id")
    fun resultById(id: Long): Single<Results>

    @Query("SELECT * FROM results")
    fun loadAllResults(): Single<List<Results>>

    @Query("SELECT * FROM results ORDER BY webPublicationDate limit :limit offset :offset")
    fun loadResults(limit: Int, offset: Int): Single<List<Results>>

    @Query("Update  results SET isSave= :isSave WHERE  id= :id")
    fun isSaveResult(id:String, isSave: Boolean)

    @Query("SELECT * FROM results WHERE isSave = :isSaved  ORDER BY webPublicationDate limit :limit offset :offset")
    fun allResultsSaved(limit: Int, offset: Int , isSaved:Boolean): Single<List<Results>>

    @Query("SELECT * FROM results ORDER BY webPublicationDate DESC limit :limit")
    fun lastResultByDate(limit: Int): Single<Results>
}