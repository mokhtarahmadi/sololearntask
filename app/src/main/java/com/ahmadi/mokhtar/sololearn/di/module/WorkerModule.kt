package com.ahmadi.mokhtar.sololearn.di.module

import androidx.work.ListenableWorker
import com.ahmadi.mokhtar.sololearn.di.annotation.WorkerKey
import com.ahmadi.mokhtar.sololearn.worker.IWorkerFactory
import com.ahmadi.mokhtar.sololearn.worker.NewsWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface WorkerModule {
    @Binds
    @IntoMap
    @WorkerKey(NewsWorker::class)
    fun bindNewsWorker(factory: NewsWorker.Factory): IWorkerFactory<out ListenableWorker>
}