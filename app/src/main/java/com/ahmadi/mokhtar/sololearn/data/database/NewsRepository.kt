package com.ahmadi.mokhtar.sololearn.data.database

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.data.network.Api
import com.ahmadi.mokhtar.sololearn.utils.isNetworkConnected
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import java.time.LocalDateTime
import javax.inject.Inject

class NewsRepository @Inject internal constructor(private val newsDao: NewsDao, private val api: Api,
                                                  private val context: Context):NewsRepo {


    override fun allResultSaved(limit: Int, offset: Int, isSaved:Boolean): Single<List<Results>> =  newsDao.allResultsSaved(limit, offset,isSaved)

    override fun isSaveResult(id:String, isSave: Boolean): Completable =Completable.fromAction { (newsDao.isSaveResult(id,isSave)) }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun periodResults(): Single<List<Results>> {
        val hasConnection = isNetworkConnected(context)
        var observableFromApi : Single<List<Results>>? = null

        if (hasConnection){
            observableFromApi =  getNewNewsFromApi()
        }

        return observableFromApi!!

    }

    override fun allResults(limit: Int, offset: Int): Observable<List<Results>> {
        val hasConnection = isNetworkConnected(context)
        var observableFromApi : Observable<List<Results>>? = null



        var page:Int = 1
        if (offset>0)
            page = (offset/10) + 1

        if (hasConnection){
            observableFromApi = getNewsFromApi(page)
        }
        var observableFromDb = getNewsFromDb(limit,offset)

        return if (hasConnection) Observable.concatArrayEager(observableFromApi,observableFromDb)
        else observableFromDb
    }


    private fun getNewsFromApi(page:Int):Observable<List<Results>>?{
        return  api.getSearchData("test","contributor",
            "starRating,headline,thumbnail,short-url","relevance",page).doOnNext{
            Timber.e(it.response?.results?.size.toString())
            for (item:Results in it.response?.results!!) {
                item?.apply {
                    newsDao.insertResults(item)
                }
            }
        }.map{ it.response?.results }
    }

    private fun getNewsFromDb(limit: Int, offset: Int):Observable<List<Results>>{
        return newsDao.loadResults(limit,offset).toObservable()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNewNewsFromApi():Single<List<Results>>{
        val toDate = LocalDateTime.now()
        var results:Single<List<Results>>? = null
        var fromDate: String? =null
        newsDao.lastResultByDate(1).subscribe({
            fromDate = it.webPublicationDate
        },{})
        if (fromDate!=null){
            results = api.getPeriodSearchData("test","starRating,headline,thumbnail,short-url",fromDate.toString(),toDate.toString(),
                "contributor","relevance").map { it.response?.results }
        }

        return results!!
    }
}