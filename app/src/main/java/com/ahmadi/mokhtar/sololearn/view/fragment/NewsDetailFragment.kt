package com.ahmadi.mokhtar.sololearn.view.fragment

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.transition.TransitionInflater
import com.ahmadi.mokhtar.sololearn.App
import com.ahmadi.mokhtar.sololearn.R
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.ahmadi.mokhtar.sololearn.utils.CustomToast
import com.ahmadi.mokhtar.sololearn.view.base.BaseFragment
import com.ahmadi.mokhtar.sololearn.viewModel.NewsDetailFragmentViewModel
import com.ahmadi.mokhtar.sololearn.viewModel.ViewModelFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_news_detail.*


class NewsDetailFragment:BaseFragment() {

    private lateinit var viewModel: NewsDetailFragmentViewModel
    private lateinit var result:Results

    companion object {


        fun newInstance(item: Results) = NewsDetailFragment().apply {
             arguments  = Bundle().apply {
                putParcelable("item", item)

            }
        }

    }
    override fun initBeforeView() {
        with(context!!.applicationContext as App){
            viewModel = androidx.lifecycle.ViewModelProviders.of(this@NewsDetailFragment , ViewModelFactory(this)).get(
                NewsDetailFragmentViewModel::class.java)
        }

        arguments?.let {
            result = it.getParcelable<Results>("item")!!
        }
    }

    override fun getContentViewId(): Int = R.layout.fragment_news_detail

    override fun initViews(rootView: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }


        var IMAGE_URI = "https://i.scdn.co/image/8d5eabf813797aa39f6e8186f702a1998d12fe40"
        if (result.tags!=null && result.tags?.size!!> 0 &&result.tags?.get(0)!=null &&
            result.tags?.get(0)?.bylineImageUrl!=null)
            IMAGE_URI = result.tags?.get(0)?.bylineImageUrl!!


        Glide.with(this).load(IMAGE_URI).centerCrop().into(newsImage)
        newsTitle.text = result.webTitle+""
        newsType.text = result.sectionName+""
        if (result.isSave)
            addToList.setImageResource(R.drawable.ic_turned_in_black_24dp)


        addToList.setOnClickListener {
            if (!result.isSave)
                addToList.setImageResource(R.drawable.ic_turned_in_black_24dp)
            else
                addToList.setImageResource(R.drawable.ic_turned_in_not_black_24dp)

            viewModel.isSaveNews(result.id,!result.isSave)
        }

        viewModelObserver()
    }


    fun viewModelObserver(){
        viewModel.isSaveResult().observe(this, Observer<Boolean>{
            result.isSave = !result.isSave
            CustomToast.makeText(context!!.applicationContext,"Saved",1)
        })

        viewModel.isSaveError().observe(this, Observer<String> {
            if (result.isSave)
                addToList.setImageResource(R.drawable.ic_turned_in_black_24dp)
            else
                addToList.setImageResource(R.drawable.ic_turned_in_not_black_24dp)

            CustomToast.makeText(context!!.applicationContext,"Error",2)
        })
    }


    override fun onDestroy() {
        viewModel.disposeElements()
        super.onDestroy()
    }

}