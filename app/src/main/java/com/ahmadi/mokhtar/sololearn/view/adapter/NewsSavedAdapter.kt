package com.ahmadi.mokhtar.sololearn.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ahmadi.mokhtar.sololearn.R
import com.ahmadi.mokhtar.sololearn.data.models.Results
import com.bumptech.glide.Glide

class NewsSavedAdapter(private val results: List<Results>, private val callback: NewsSavedAdapter.Callback) : RecyclerView.Adapter<NewsSavedAdapter.ViewHolder>() {


    private var mList = ArrayList<Results>()

    init {
        this.mList = results as java.util.ArrayList<Results>
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NewsSavedAdapter.ViewHolder {
        val v  = LayoutInflater.from(viewGroup.context).
            inflate(R.layout.news_save_item,viewGroup,false);
        return ViewHolder(v)
    }


    fun addNews(results: List<Results>) {
        val initPosition = mList.size
        mList = results as ArrayList<Results>
        notifyItemRangeInserted(initPosition, mList.size)
    }
    override fun getItemCount(): Int = mList.size

    override fun onBindViewHolder(holder: NewsSavedAdapter.ViewHolder, position: Int) {

        var IMAGE_URI = "https://i.scdn.co/image/8d5eabf813797aa39f6e8186f702a1998d12fe40"
        if (mList.get(position).tags!=null && (mList.get(position).tags?.size!!> 0) &&mList.get(position).tags?.get(0)!=null &&
            mList.get(position).tags?.get(0)?.bylineImageUrl!=null)
            IMAGE_URI = mList.get(position).tags?.get(0)?.bylineImageUrl!!


        Glide.with(holder.itemView.context).load(IMAGE_URI).centerCrop().into(holder.newsImage)
        holder.pillarName.setText(mList.get(position).pillarName+"")

        holder.itemView.setOnClickListener {
            callback.onItemClick(mList.get(position), holder.newsImage,holder.pillarName)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var pillarName: TextView
        var newsImage: ImageView

        init {

            pillarName = itemView.findViewById(R.id.pillarName) as TextView
            newsImage = itemView.findViewById(R.id.newsImage) as ImageView
        }
    }


    interface Callback{
        fun onItemClick(item: Results, imageView: ImageView, textView: TextView)
    }
}