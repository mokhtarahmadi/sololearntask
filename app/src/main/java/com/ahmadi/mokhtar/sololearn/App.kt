package com.ahmadi.mokhtar.sololearn

import android.app.Activity
import android.app.Application
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.work.Configuration
import androidx.work.WorkManager
import com.ahmadi.mokhtar.sololearn.di.component.DIComponent
import com.ahmadi.mokhtar.sololearn.di.component.DaggerDIComponent
import com.ahmadi.mokhtar.sololearn.di.module.ApiModule
import com.ahmadi.mokhtar.sololearn.di.module.AppModule
import com.ahmadi.mokhtar.sololearn.worker.NewsWorkerFactory

class App:Application() {

    lateinit var di:DIComponent

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {

            }

            override fun onActivityResumed(activity: Activity?) {

            }

            override fun onActivityStarted(activity: Activity?) {

            }

            override fun onActivityDestroyed(activity: Activity?) {

            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

            }

            override fun onActivityStopped(activity: Activity?) {

            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        })

        di = DaggerDIComponent
            .builder()
            .apiModule(ApiModule())
            .appModule(AppModule(this))
            .build()







        var workerFactory: NewsWorkerFactory =  di.inject()


        val config = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
        WorkManager.initialize(this, config)
    }
}