package com.ahmadi.mokhtar.sololearn.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ahmadi.mokhtar.sololearn.data.models.Results

@Database(entities = [Results::class], version = 1)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun getNewsDao():NewsDao
}