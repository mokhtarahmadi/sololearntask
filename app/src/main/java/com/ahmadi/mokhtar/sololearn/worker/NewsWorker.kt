package com.ahmadi.mokhtar.sololearn.worker

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.ahmadi.mokhtar.sololearn.data.database.NewsRepo
import com.ahmadi.mokhtar.sololearn.utils.Constants.Companion.CHANNEL_ID
import com.ahmadi.mokhtar.sololearn.utils.Constants.Companion.NOTIFICATION_ID
import com.ahmadi.mokhtar.sololearn.utils.Constants.Companion.VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION
import com.ahmadi.mokhtar.sololearn.utils.Constants.Companion.VERBOSE_NOTIFICATION_CHANNEL_NAME
import com.ahmadi.mokhtar.sololearn.utils.displayNotification
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider

class NewsWorker @Inject constructor(private var context: Context, private var params: WorkerParameters, private var newsRepo: NewsRepo): Worker(context,params){

    @SuppressLint("RestrictedApi")
    override fun doWork(): Result  = try {
        newsRepo.periodResults().subscribe({
            if (it.size>0){
                displayNotification("News" ,"${it.size}  new News", context )
            }
        },{
            Result.Retry()
        })

        Result.success()

    }catch (e:Throwable){
        Timber.e(e.message)
        Result.failure()
    }



    class Factory @Inject constructor(private val newsRepo: Provider<NewsRepo>) : IWorkerFactory<NewsWorker> {
        override fun create(context: Context,params: WorkerParameters): NewsWorker {
            return NewsWorker(context, params, newsRepo.get())
        }
    }
}