package com.ahmadi.mokhtar.sololearn.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Guardian(
    val response: Response?
): Parcelable